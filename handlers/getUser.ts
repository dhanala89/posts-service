import { APIGatewayProxyEvent, Callback, Context, Handler } from "aws-lambda";
import { AwsResponseHelper } from "../common/awsResponseHelper";
var request = require("request");

function getUserDetails(queryParams: any) {
  return new Promise(function (resolve, reject) {
    var options = {
      method: "GET",
      url: `https://jsonplaceholder.typicode.com/users/${
        queryParams ? queryParams.id : ""
      }`,
      headers: {
        "Content-Type": "application/json",
      },
    };
    request(options, function (error: any, response: any, body: any) {
      // in addition to parsing the value, deal with possible errors
      if (error) return reject(error);
      try {
        // JSON.parse() can throw an exception if not valid JSON
        resolve(JSON.parse(body));
      } catch (e) {
        reject(e);
      }
    });
  });
}

export const handler: Handler = async (
  event: APIGatewayProxyEvent,
  context: Context,
  cb: Callback
) => {
  try {
    let qp = event.queryStringParameters;

    let userDetails: any = await getUserDetails(qp);

    AwsResponseHelper.callbackRespondWithJsonBody(cb, 200, userDetails);
  } catch (err) {
    if (err.statusCode) {
      AwsResponseHelper.callbackRespondWithSimpleMessage(
        cb,
        err.statusCode,
        err.message
      );
    } else {
      AwsResponseHelper.callbackRespondWithSimpleMessage(
        cb,
        500,
        "Internal Server error"
      );
    }
  }
};
