{
  "swagger": "2.0",
  "info": {
    "description": "This is a posts service API. For this, you can use the api key `special-key` to test the authorization filters.",
    "version": "1.0.0",
    "title": "Swagger Posts",
    "termsOfService": "http://swagger.io/terms/",
    "contact": {
      "email": "sharanraj.d@gmail.com"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "host": "y4ti6friqi.execute-api.ap-southeast-2.amazonaws.com",
  "basePath": "/dev",
  "tags": [
    {
      "name": "post",
      "description": "Everything about posts"
    },
    {
      "name": "comment",
      "description": "Everything about comment"
    },
    {
      "name": "user",
      "description": "Everything about user"
    }
  ],
  "schemes": ["https", "http"],
  "paths": {
    "/posts": {
      "get": {
        "tags": ["post"],
        "summary": "Get all posts",
        "description": "Returns all available posts",
        "operationId": "getAllPosts",
        "produces": ["application/json"],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Post"
              }
            }
          },
          "403": {
            "description": "Forbiddden"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "x-swagger-router-controller": "Post",
        "security": [
          {
            "x-api-key": []
          }
        ]
      }
    },
    "/posts/{id}": {
      "get": {
        "tags": ["post"],
        "summary": "Find post by ID",
        "description": "Returns a single post",
        "operationId": "getPostById",
        "produces": ["application/json"],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of post to return",
            "required": true,
            "type": "integer"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "$ref": "#/definitions/Post"
            }
          },
          "403": {
            "description": "Forbiddden"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "security": [
          {
            "x-api-key": []
          }
        ],
        "x-swagger-router-controller": "Post"
      }
    },
    "/comments": {
      "get": {
        "tags": ["comment"],
        "summary": "Get all comments",
        "description": "Returns all available comments",
        "operationId": "getAllComments",
        "produces": ["application/json"],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Comment"
              }
            }
          },
          "403": {
            "description": "Forbiddden"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "x-swagger-router-controller": "Comment"
      }
    },
    "/comments/{id}": {
      "get": {
        "tags": ["comment"],
        "summary": "Find appropriate comments for post ID",
        "description": "Returns appropriate comments",
        "operationId": "getCommentById",
        "produces": ["application/json"],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of post to return",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Comment"
              }
            }
          },
          "403": {
            "description": "Forbiddden"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "security": [
          {
            "x-api-key": []
          }
        ],
        "x-swagger-router-controller": "Comment"
      }
    },
    "/users": {
      "get": {
        "tags": ["user"],
        "summary": "Get all users",
        "description": "Returns all users",
        "operationId": "getAllUsers",
        "produces": ["application/json"],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/User"
              }
            }
          },
          "403": {
            "description": "Forbiddden"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "x-swagger-router-controller": "User"
      }
    },
    "/users/{id}": {
      "get": {
        "tags": ["user"],
        "summary": "Find user by ID",
        "description": "Returns user by id",
        "operationId": "getUserById",
        "produces": ["application/json"],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of user to return",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "$ref": "#/definitions/User"
            }
          },
          "403": {
            "description": "Forbiddden"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "security": [
          {
            "x-api-key": []
          }
        ],
        "x-swagger-router-controller": "User"
      }
    }
  },
  "securityDefinitions": {
    "x-api-key": {
      "type": "apiKey",
      "name": "x-api-key",
      "in": "header"
    }
  },
  "definitions": {
    "Post": {
      "type": "object",
      "required": ["userId", "id", "title", "body"],
      "properties": {
        "userId": {
          "type": "integer",
          "format": "int64"
        },
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "title": {
          "type": "string"
        },
        "body": {
          "type": "string"
        }
      },
      "example": {
        "userId": 1,
        "id": 0,
        "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehender"
      }
    },
    "Comment": {
      "type": "object",
      "required": ["postId", "id", "name", "email", "body"],
      "properties": {
        "postId": {
          "type": "integer",
          "format": "int64"
        },
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "name": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "body": {
          "type": "string"
        }
      },
      "example": {
        "postId": 1,
        "id": 1,
        "name": "id labore ex et quam laborum",
        "email": "Eliseo@gardner.biz",
        "body": "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
      }
    },
    "Address": {
      "type": "object",
      "properties": {
        "street": {
          "type": "string"
        },
        "suite": {
          "type": "string"
        },
        "city": {
          "type": "string"
        },
        "zipcode": {
          "type": "string"
        },
        "geo": {
          "type": "object",
          "properties": {
            "lat": {
              "type": "string"
            },
            "lng": {
              "type": "string"
            }
          }
        }
      }
    },
    "User": {
      "type": "object",
      "required": ["id", "name", "username", "email", "address"],
      "properties": {
        "postId": {
          "type": "integer",
          "format": "int64"
        },
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "name": {
          "type": "string"
        },
        "username": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "address": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Address"
          }
        }
      },
      "example": {
        "postId": 1,
        "id": 1,
        "name": "id labore ex et quam laborum",
        "username": "Bret",
        "email": "Eliseo@gardner.biz",
        "address": {
          "street": "Kulas Light",
          "suite": "Apt. 556",
          "city": "Gwenborough",
          "zipcode": "92998-3874",
          "geo": {
            "lat": "-37.3159",
            "lng": "81.1496"
          }
        },
        "phone": "1-770-736-8031 x56442",
        "website": "hildegard.org",
        "company": {
          "name": "Romaguera-Crona",
          "catchPhrase": "Multi-layered client-server neural-net",
          "bs": "harness real-time e-markets"
        }
      }
    }
  },
  "externalDocs": {
    "description": "Find out more about Swagger",
    "url": "http://swagger.io"
  }
}
