import { Callback } from "aws-lambda";

("use strict");

class AwsResponseHelper {
  static callbackRespondWithCodeOnly = function (
    callback: Callback,
    code: number
  ) {
    callback(null, {
      statusCode: code,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
    });
  };

  static callbackRespondWithSimpleMessage = function (
    callback: Callback,
    code: number,
    message: String
  ) {
    callback(null, {
      statusCode: code,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        message: message,
      }),
    });
  };

  static callbackRespondWithJsonBody = function (
    callback: Callback,
    code: number,
    body: Object
  ) {
    callback(null, {
      statusCode: code,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(body),
    });
  };
}

export { AwsResponseHelper };
